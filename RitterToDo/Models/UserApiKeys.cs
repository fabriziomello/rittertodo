﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace RitterToDo.Models
{
    public class UserApiKey
    {
        [Key]
        [StringLength(100, MinimumLength = 4)]
        public string ApiKey { get; set; }

        [Required]
        [StringLength(100, MinimumLength = 4)]
        public string AppName { get; set; }

        [Required]
        [ForeignKey("Owner")]
        public string OwnerId { get; set; }

        public ApplicationUser Owner { get; set; }
    }

    public class UserApiViewModel
    {
        public string ApiKey { get; set; }

        public string AppName { get; set; }

        public string OwnerId { get; set; }
    }
}
