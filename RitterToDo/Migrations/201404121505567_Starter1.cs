namespace RitterToDo.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Starter1 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.ToDoViewModel",
                c => new
                    {
                        ToDoViewModelId = c.Guid(nullable: false),
                        Name = c.String(),
                        Description = c.String(),
                        DueDate = c.DateTime(),
                        Starred = c.Boolean(nullable: false),
                        CategoryName = c.String(),
                        Done = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.ToDoViewModelId);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.ToDoViewModel");
        }
    }
}
