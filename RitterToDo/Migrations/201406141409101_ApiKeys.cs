namespace RitterToDo.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ApiKeys : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.UserApiKey",
                c => new
                    {
                        ApiKey = c.String(nullable: false, maxLength: 100),
                        AppName = c.String(nullable: false, maxLength: 100),
                        OwnerId = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => t.ApiKey)
                .ForeignKey("dbo.AspNetUsers", t => t.OwnerId, cascadeDelete: true)
                .Index(t => t.OwnerId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.UserApiKey", "OwnerId", "dbo.AspNetUsers");
            DropIndex("dbo.UserApiKey", new[] { "OwnerId" });
            DropTable("dbo.UserApiKey");
        }
    }
}
